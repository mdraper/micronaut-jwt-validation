package com.owlbrite.upvotes.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.Secured;
import static io.micronaut.http.HttpResponse.ok;

@Controller("/upvotes")

public class UpvotesController {

    @Get("/")
    @Secured("isAnonymous()")
    public String index() {
        return "Hello World";
    }

    @Post("/{postId}")
    @Secured("isAuthenticated()")
    public HttpResponse<Boolean> upvotePost(Integer postId){
        return ok(false);
    }

}
