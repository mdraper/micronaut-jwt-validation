package com.owlbrite.upvotes;

import com.nimbusds.jose.JWSAlgorithm;
import io.micronaut.context.annotation.Bean;
import io.micronaut.runtime.Micronaut;
import io.micronaut.security.token.jwt.signature.secret.SecretSignatureConfiguration;

import javax.inject.Singleton;

public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}